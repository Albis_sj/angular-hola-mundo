import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  frase: any = { //es un json, entonces se le pone any como tipado, o creamos una clse que se llame frase, con esos atributor, y ya seria de tipo nombre de la clase (:frase)
    mensaje: 'La sabiduría viene de la experiencia. La experiencia es, a menudo, el resultado de la falta de sabiduría',
    autor: 'Terry Pratchett'
  }

  mostrar= true;
  valorNumerico: number =  3;
  personas: string[] = ['Pedro', 'Carlos', 'Maria'];

  constructor() { }

  ngOnInit(): void {
  }

}
